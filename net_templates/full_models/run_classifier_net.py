from __future__ import division, print_function

import torchvision.transforms as transforms
from src.utils import *
from src.read_mnist import read_train_test_mnist
from src.datafeed import *
import time, sys
from net import Net


np.random.seed(1337)  # for reproducibility

cprint('c','\nData:')

# this reads in numpy
x_train, y_train, x_dev, y_dev = read_train_test_mnist()

x_train = np.reshape(x_train,(-1,1,28,28))
x_dev = np.reshape(x_dev,(-1,1,28,28))
x_train = np.transpose(x_train, (0, 2, 3, 1))
x_dev = np.transpose(x_dev, (0, 2, 3, 1))
print(x_train.shape)

y_train = np.argmax(y_train, axis=1)
y_dev = np.argmax(y_dev, axis=1)

print('  x_train: %d' % len(x_train))
print('  x_dev: %d' % len(x_dev))

## ---------------------------------------------------------------------------------------------------------------------
# train config
nb_epochs = 130
batch_size = 128

## ---------------------------------------------------------------------------------------------------------------------
transform_train = transforms.Compose([
        #transforms.RandomCrop(28, padding=4),
        transforms.ToTensor(),
    ])

transform_test = transforms.Compose([
    transforms.ToTensor(),
])

trainset = Datafeed(x_train, y_train, transform=transform_train)
trainloader = data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=3)

testset = Datafeed(x_dev, y_dev, transform=transform_test)
testloader = data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=3)

## ---------------------------------------------------------------------------------------------------------------------
# net dims
input_dim = 1 #channels
output_dim = np.max(y_train) + 1

print('  input_dim: %s' % str(input_dim))
print('  output_dim: %d' % output_dim)

# --------------------
use_cuda = torch.cuda.is_available()
net = Net(input_dim, output_dim, lr=1e-4, cuda=use_cuda)

## ---------------------------------------------------------------------------------------------------------------------
# train
cprint('c','\nTrain:')


err_train = np.zeros(nb_epochs)
cost_train = np.zeros(nb_epochs)
cost_dev = np.zeros(nb_epochs)
err_dev = np.zeros(nb_epochs)

nb_samples_train = len(x_train)
nb_samples_dev = len(x_dev)

best_cost = np.inf
nb_its_dev = 1
tic0 = time.time()
for i in range(nb_epochs):
    net.set_mode_train(True)
    # ---- W
    tic = time.time()
    for x, y in trainloader:
        loss, err = net.fit(x, y, i)
        cost_train[i] += loss / nb_samples_train * len(x)
        err_train[i] += err / nb_samples_train

    toc = time.time()

    # ---- print
    print("it %d/%d, Jtr = %f, err = %f, " % (i, nb_epochs, cost_train[i],err_train[i]), end="")
    cprint('r','   time: %f seconds\n' % (toc - tic))
    net.update_lr(i)

    # ---- dev
    if i % nb_its_dev == 0:
        net.set_mode_train(False)
        #print('eval mode on')
        for x, y in testloader:
        #for ind in generate_ind_batch(nb_samples_dev, batch_size, random=False):
            #x, y = x_dev[ind], y_dev[ind]
            cost, err = net.eval(x, y)
            cost_dev[i] += cost / nb_samples_dev * len(x)
            err_dev[i] += err / nb_samples_dev
        cprint('g','    Jdev = %f, err = %f\n' % (cost_dev[i], err_dev[i]))
        if cost_dev[i] < best_cost:
            best_cost = cost_dev[i]
            net.save('./theta_best.dat')

toc0 = time.time()
runtime_per_it =  (toc0 - tic0)/float(nb_epochs)
cprint('r','   average time: %f seconds\n' % runtime_per_it)

## ---------------------------------------------------------------------------------------------------------------------
# save model
net.save('./theta_last.dat')

## ---------------------------------------------------------------------------------------------------------------------
# results
cprint('c','\nRESULTS:')
cost_dev_min = cost_dev[::nb_its_dev].min()
err_dev_min = err_dev[::nb_its_dev].min()
cost_train_min = cost_train.min()
nb_parameters = net.get_nb_parameters()
print('  cost_dev: %f (cost_train %f)' % (cost_dev_min, cost_train_min))
print('  err_dev: %f' % (err_dev_min))
print('  nb_parameters: %d (%s)' % (nb_parameters, humansize(nb_parameters)))
print('  time_per_it: %fs\n' % (runtime_per_it))

with open('./results.txt','w') as f:
    f.write('%f %f %d %s %f\n' % (err_dev_min, cost_dev_min, nb_parameters, humansize(nb_parameters), runtime_per_it))