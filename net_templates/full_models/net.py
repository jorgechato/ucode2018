import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.nn import functional as F
from torch.optim.optimizer import Optimizer, required
import torch.backends.cudnn as cudnn
from src.layers_pytorch import *
from src.utils import *
from src.models import *
import numpy as np

class CenterLossUnsupervised(nn.Module):
    def __init__(self, nb_clust, dim, loss_weight=1e-2, alpha=0.05):
        super(CenterLossUnsupervised, self).__init__()
        self.nb_clust = nb_clust
        self.dim = dim
        self.loss_weight = loss_weight
        self.alpha = alpha
        self.register_buffer('centers', 0.1 * torch.randn(self.nb_clust, self.dim))

    def forward(self, x):
        # x(n_batch, features_dim)
        # y(n_batch) -> vector de pertenencia de elemento cluster
        # centers(nClsuters, features_dim)
        batch_size = x.size(0)
        features_dim = x.size(1)

        centers_var = Variable(self.centers)
        # diff (batch_size, N_clusters, features_dim)
        diff_1 = centers_var.unsqueeze(0) - x.unsqueeze(1)
        # min_idx (batch_size)
        _, y = diff_1.pow(2).sum(2, keepdim=False).min(dim=1, keepdim=False)

        # TODO: slice dif with min_idx, finish algorithm, change to AM style

        # y_expand(batch_size,features_dim)
        y_expand = y.view(batch_size, 1).expand(batch_size, features_dim)

        # centers(n_centers, features_dim)

        # centers_batch(batch_size, features_dim) -> center values corresponding to each batch element
        # y_expand contains the index of the correct class repeated over features dim. This values is used as an index
        # for centers_var which is (n_centers=N_classes, features_dim) over dimension 0. So we select centers var[y_i,y_i]
        # for each element in the batch: the two dimensions of each center corresponding to each element in batch
        centers_batch = centers_var.gather(0, y_expand)#.cuda()
        if x.is_cuda:
            centers_batch = centers_batch.cuda()

        criterion = nn.MSELoss()
        # loss(1). Mean squared error across dims and batch elements
        loss = criterion(x, centers_batch)
        if self.training:  #
            # diff(batch_size, features_dim)
            diff = centers_batch - x

            # get amount of points in batch belonging to each center
            # ** I think ** -> appear_times(batch_size) contains number of appearances of each sample's center
            unique_label, unique_reverse, unique_count = np.unique(y.cpu().data.numpy(), return_inverse=True,
                                                                   return_counts=True)
            appear_times = torch.from_numpy(unique_count).gather(0, torch.from_numpy(unique_reverse))

            # appear_times_expand(batch_size,features_dim)
            appear_times_expand = appear_times.view(-1, 1).expand(batch_size, features_dim).type(torch.FloatTensor)
            # normalise diff by times a class apears
            diff_cpu = diff.cpu().data / appear_times_expand.add(1e-6)
            diff_cpu = self.alpha * diff_cpu
            for i in range(batch_size):
                self.centers[y.data[i]] -= diff_cpu[i].type(self.centers.type())

        return self.loss_weight * loss


# class CenterLossUnsupervised(nn.Module):
#     def __init__(self, nb_clust, dim, loss_weight=1e-2, alpha=0.05):
#         super(CenterLossUnsupervised, self).__init__()
#         self.nb_clust = nb_clust
#         self.dim = dim
#         self.loss_weight = loss_weight
#         self.alpha = alpha
#         self.register_buffer('centers', 0.1 * torch.randn(self.nb_clust, self.dim))

#     def forward(self, x):
#         # x(n_batch, features_dim)
#         # y(n_batch) -> vector de pertenencia de elemento cluster
#         # centers(nClsuters, features_dim)

#         centers = Variable(self.centers, requires_grad=False) #no gradient to centers
#         # diff (batch_size, N_clusters, features_dim)
#         diff_1 = centers.unsqueeze(0) - x.unsqueeze(1)
#         # min_idx (batch_size)
#         _, y = diff_1.pow(2).sum(2, keepdim=False).min(dim=1, keepdim=False)

#         c = centers.index_select(0, y)
#         diff = c - x
#         loss = 0.5 * self.loss_weight * (diff.pow(2).sum(1)).mean()
#         if self.training:
#             batch_size = x.size(0)
#             z = Variable(torch.zeros(batch_size, self.nb_clust), requires_grad=False)
#             if y.is_cuda:
#                 z = z.cuda()
#             z.scatter_(1, y.view(-1, 1), 1)
#             n = z.sum(0) + 1
#             for i in range(batch_size):
#                 centers[y[i].data] -= self.alpha * diff[i] / n[y[i]]

#         return loss

class CenterLoss(nn.Module):
    def __init__(self, nb_class, dim, loss_weight=1e-2, alpha=0.05):
        super(CenterLoss, self).__init__()
        self.nb_class = nb_class
        self.dim = dim
        self.loss_weight = loss_weight
        self.alpha = alpha
        self.register_buffer('centers', torch.zeros(self.nb_class, self.dim))

    def forward(self, x, y):
        centers = Variable(self.centers, requires_grad=False)
        c = centers.index_select(0, y)
        diff = c - x
        loss = self.loss_weight * (diff.pow(2).sum(1)).mean()
        if self.training:
            batch_size = x.size(0)
            z = Variable(torch.zeros(batch_size, self.nb_class), requires_grad=False)
            if y.is_cuda:
                z = z.cuda()
            z.scatter_(1, y.view(-1, 1), 1)
            n = z.sum(0) + 1
            for i in range(batch_size):
                centers[y[i].data] -= self.alpha * diff[i] / n[y[i]]

        return loss


class Convbn(nn.Module):
    def __init__(self, input_dim, output_dim):
        super(Convbn, self).__init__()
        self.conv1 = nn.Conv2d(input_dim, 16, kernel_size=5, padding=2)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, padding=2)

        self.bn1 = nn.BatchNorm2d(32)
        self.fc1 = nn.Linear(32 * 7 * 7, 10)
        self.fc2 = nn.Linear(10, 10)
        self.output_dim = output_dim

        self.CU = CenterLossUnsupervised(10, 16, loss_weight=1.0, alpha=0.01)

    def forward(self, x):
        x = self.conv1(x)
        x = F.max_pool2d(x, 2)
        # print(x.shape)
        e = x.permute(0, 2, 3, 1).contiguous().view(-1, 16)
        x = relu(x)
        # -----
        x = self.conv2(x)
        x = F.max_pool2d(x, 2)
        x = relu(x)
        # -----
        x = self.bn1(x)
        x = x.view(-1, 32 * 7 * 7)
        # -----
        x = self.fc1(x)
        x = relu(x)
        # -----
        x = self.fc2(x)
        return x, e


class Net(BaseNet):
    def __init__(self, input_dim, output_dim, lr=1e-4, cuda=True):
        super(Net, self).__init__()
        cprint('c', '\nNet:')
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.cuda = cuda
        self.create_net()
        self.create_opt(lr)

    def create_net(self):
        self.model = Convbn(self.input_dim, self.output_dim)
        self.J = nn.CrossEntropyLoss()
        #         self.C2 = CenterLoss2(self.output_dim, 2, loss_weight=1.0, alpha=0.01).cuda()
        #         self.C = CenterLoss(self.output_dim, 5, loss_weight=1.0, alpha=0.01).cuda() # changed to 5 dims
        if self.cuda:
            self.model.cuda()
            self.J.cuda()

        print('    Total params: %.2fM' % (sum(p.numel() for p in self.model.parameters()) / 1000000.0))

    def create_opt(self, lr=1e-4):
        self.lr = lr
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.lr)
        self.schedule = None  # [-1] #[50,200,400,600]

    def fit(self, x, y, epoch):
        x, y = to_variable(var=(x, y), cuda=self.cuda)

        self.optimizer.zero_grad()
        out, e = self.model(x)

        loss = self.J(out, y) + self.model.CU(e)

        loss.backward()
        self.optimizer.step()

        pred = out.data.max(1)[1]
        err = pred.ne(y.data).cpu().sum()

        return loss.data[0], err

    def eval(self, x, y, train=False):
        x, y = to_variable(var=(x, y), volatile=True, cuda=self.cuda)

        out, e = self.model(x)
        loss = self.J(out, y) + self.model.CU(e)

        pred = out.data.max(1)[1]
        err = pred.ne(y.data).cpu().sum()

        return loss.data[0], err

    def predict(self, x, train=False):
        x, = to_variable(var=(x,), volatile=True, cuda=self.cuda)

        out, _ = self.model(x)
        return out.data

    def extract(self, x, train=False):
        x, = to_variable(var=(x,), volatile=True, cuda=self.cuda)

        _, e = self.model(x)
        return e.data