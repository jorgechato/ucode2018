﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using bananaforscale.CustomAtributes;
using bananaforscale.Helpers;
using bananaforscale.Models;
using bananaforscale.Models.Requests;
using bananaforscale.Models.Tasks;
using bananaforscale.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace bananaforscale.Controllers
{
    [Produces("application/json")]
    [Route("api/Dimitri")]
    public class DimitriController : Controller
    {
        private readonly IHostingEnvironment _appEnvironment;

        private readonly IRabbitService _rservice;

        public DimitriController(IHostingEnvironment appEnvironment, IRabbitService rservice)
        {
            _appEnvironment = appEnvironment;
            _rservice = rservice;
        }

        //[HttpGet]
        //public async Task<IActionResult> Get()
        //{
        //    TaskModel result = null;
        //    try
        //    {
        //        var model = new GenerateTextureRequestModel { Cloth = "que pasa co: " + DateTime.Now };
        //        result = await _rservice.SendTask("", "dimitri", model, 30000);
        //    }
        //    finally
        //    {
        //        //Liberamos la task
        //        _rservice.ReleaseTask(result.Id);
        //    }
            
        //    if (result == null && result.TimedOut)
        //    {
        //        return Ok("TIMED OUT");
        //    }

        //    //Devolvemos el JSON tal cual
        //    return Content(result.Json, "application/json");
        //}

        [HttpPost("imagine/{cloth}")]
        [DisableFormValueModelBinding]
        public async Task<IActionResult> Post(string cloth, List<IFormFile> files)
        {
            if (!files.Any())
            {
                return Json(new { msg = "No files uploaded, skipping." });
            }

            int taskId = _rservice.ReserveTaskId();

            // full path to file in temp location
            string filePathRoot = Path.Combine(_appEnvironment.WebRootPath, "shared");

            var model = new GenerateTextureRequestModel
            {
                Cloth = cloth,
                Files = new List<string>()
            };

            int count = 0;
            foreach (var formFile in files)
            {
                string filename = $"{taskId}_{count}{Path.GetExtension(formFile.FileName)}";
                string filepath = Path.Combine(filePathRoot, filename);

                model.Files.Add(filename);

                if (formFile.Length > 0)
                {
                    using (var stream = new FileStream(filepath, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
                count++;
            }

            TaskModel result = null;
            try
            {
                result = await _rservice.SendTask("", "imagine", model, 30000);
            }
            finally
            {
                //Liberamos la task
                _rservice.ReleaseTask(result.Id);
            }

            if (result == null && result.TimedOut)
            {
                return Ok("TIMED OUT");
            }

            //Devolvemos el JSON tal cual
            return Content(result.Json, "application/json");
        }

        //Post de imágenes
        //[HttpPost("UploadFiles")]
        /// <summary>
        /// Posteamos una ropa que queremos convertir en otra
        /// </summary>
        /// <param name="cloth"></param>
        /// <returns></returns>
        //[HttpPost("imagine/{cloth}")]
        //[DisableFormValueModelBinding]
        //public async Task<IActionResult> Post(string cloth)
        //{
        //    string filename = Path.GetRandomFileName();
        //    string filePath = Path.Combine(_appEnvironment.WebRootPath, "shared", filename);

        //    FormValueProvider formModel;
        //    using (var stream = System.IO.File.Create(filePath))
        //    {
        //        formModel = await Request.StreamFile(stream);
        //    }

        //    var viewModel = new ClothRequestModel();

        //    var bindingSuccessful = await TryUpdateModelAsync(viewModel, prefix: "",valueProvider: formModel);

        //    if (!bindingSuccessful)
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            return BadRequest(ModelState);
        //        }
        //    }

        //    TaskModel result = null;
        //    try
        //    {
        //        var model = new GenerateTextureRequestModel {

        //            Cloth = cloth,
        //            Files = new List<string>()
        //        };

        //        result = await _rservice.SendTask("", "dimitri", model, 30000);
        //    }
        //    finally
        //    {
        //        //Liberamos la task
        //        _rservice.ReleaseTask(result.Id);
        //    }

        //    if (result == null && result.TimedOut)
        //    {
        //        return Ok("TIMED OUT");
        //    }

        //    //Devolvemos el JSON tal cual
        //    return Content(result.Json, "application/json");
        //}

        //[HttpPost("texture/{id}/{cloth}")]
        //[DisableFormValueModelBinding]
        //public async Task<IActionResult> PostTexture(int id, string cloth)
        //{
        //    string filename = id.ToString() + "_" + cloth + ".jpg";
        //    string filePath = Path.Combine(_appEnvironment.WebRootPath, "assets", filename);

        //    FormValueProvider formModel;
        //    using (var stream = System.IO.File.Create(filePath))
        //    {
        //        formModel = await Request.StreamFile(stream);
        //    }

        //    var viewModel = new ClothRequestModel();

        //    var bindingSuccessful = await TryUpdateModelAsync(viewModel, prefix: "", valueProvider: formModel);

        //    if (!bindingSuccessful)
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            return BadRequest(ModelState);
        //        }
        //    }

        //    try
        //    {
        //        string json = $"{{\"msg\":\"Texture generated\"}}";
        //        _rservice.ResponseTask(id, json);
        //    }
        //    catch
        //    {

        //    }
        //    return Json(new { msg = "Ok", model = viewModel });
        //}

        /// <summary>
        /// Carga de bloque de texturas
        /// </summary>
        /// <param name="id"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        [HttpPost("textures/{id}/{cloth}")]
        public async Task<IActionResult> PostTextures(int id, string cloth, List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);

            // full path to file in temp location
            string filePathRoot = Path.Combine(_appEnvironment.WebRootPath, "assets");

            var fileList = new List<string>();
            
            int count = 0;
            foreach (var formFile in files)
            {
                string filename = $"{id}_{count}_{cloth}{Path.GetExtension(formFile.FileName)}";
                string filepath = Path.Combine(filePathRoot, filename);

                fileList.Add(filename);

                if (formFile.Length > 0)
                {
                    using (var stream = new FileStream(filepath, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
                count++;
            }

            try
            {
                _rservice.ResponseTask2(id, fileList);
            }
            catch
            {

            }
            return Json(new { files = files.Count, length = size });
            //Devolvemos el JSON tal cual
        }

        /// <summary>
        /// Realizamos una busqueda de ROIS en los archivos subidos
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        [HttpPost("find")]
        public async Task<IActionResult> FindClothes(List<IFormFile> files)
        {
            if (!files.Any())
            {
                return Json(new { msg = "No files uploaded, skipping." });
            }

            int taskId = _rservice.ReserveTaskId();

            // full path to file in temp location
            string filePathRoot = Path.Combine(_appEnvironment.WebRootPath, "shared");

            var model = new FindClothesRequestModel
            {
                Id = taskId,

                Files = new List<string>()
            };
            
            int count = 0;
            foreach (var formFile in files)
            {
                string filename = $"{taskId}_{count}{Path.GetExtension(formFile.FileName)}";
                string filepath = Path.Combine(filePathRoot, filename);

                model.Files.Add(filename);

                if (formFile.Length > 0)
                {
                    using (var stream = new FileStream(filepath, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
                count++;
            }

            TaskModel result = null;
            try
            {
                result = await _rservice.SendTask("", "find", model, 30000, taskId);
            }
            finally
            {
                //Liberamos la task
                _rservice.ReleaseTask(result.Id);
            }

            if (result == null || result.TimedOut)
            {
                return Ok("\"{msg\": \"TIMED OUT\"");
            }

            //Devolvemos el JSON tal cual
            return Content(result.Json, "application/json");
        }
    }
}