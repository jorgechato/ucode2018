﻿using bananaforscale.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace bananaforscale.Services
{
    public interface IRabbitService
    {
        int ReserveTaskId();

        Task<TaskModel> SendTask(string exchange, string queue, object model, int timeout, int reservedTaskId );
        Task<TaskModel> SendTask(string exchange, string queue, object model, int timeout);

        void ResponseTask(int id, string json);
        void ResponseTask2(int id, object obj);

        void ReleaseTask(int id);
    }


    public class RabbitService : IRabbitService
    {
        private readonly ILogger _logger;

        ConnectionFactory _rfactory;
        IConnection _rconnection;

        //Respuestas
        IModel _responseChannel;

        EventingBasicConsumer _rconsumer;

        Dictionary<int, TaskModel> _tasks = new Dictionary<int, TaskModel>();

        int _Id = 0;

        Regex _idRegex;

        JsonSerializerSettings _jsonSerializer;

        public RabbitService(ILogger<RabbitService> logger)
        {
            _logger = logger;

            _rfactory = new ConnectionFactory
            {
                //Servidor
                HostName = "localhost",
                //HostName = "aitgelionr.ddns.net",
                Port = 5600,

                VirtualHost ="/woodenCNC",
                
                //Credenciales
                UserName = "pi",
                Password = "woodenCNC"
            };

            _idRegex = new Regex("\"id\" *: *(\\d+)", RegexOptions.Compiled);

            _jsonSerializer = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            InitRabbit();
        }

        private void InitRabbit()
        {
            //Creamos los channels, etc
            _rconnection = _rfactory.CreateConnection();
            _responseChannel = _rconnection.CreateModel();

            //Declaramos el exchange
            //_rchannel.ExchangeDeclare("response", ExchangeType.Fanout, false);

            //Declaramos la cola de respuestas
            _responseChannel.QueueDeclare(
                queue: "result",
                //durable: false,                    
                exclusive: false
                //autoDelete: true,
                //arguments: null
            );
            
            //Preparamos el consumer
            _rconsumer = new EventingBasicConsumer(_responseChannel);
            _responseChannel.BasicConsume("result", true, _rconsumer);

            _rconsumer.Received += ProcessRabbitMsg;
        }

        private void ProcessRabbitMsg(object model, BasicDeliverEventArgs ea)
        {
            //Obtenemos el JSON devuelto
            var message = Encoding.UTF8.GetString(ea.Body);
            
            //Obtenemos el identificador de la tarea
            var match = _idRegex.Match(message);
            if (!match.Success) return;

            int id = int.Parse(match.Groups[1].Value);

            TaskModel task = null;
            _tasks.TryGetValue(id, out task);

            if (task != null)
            {
                task.Json = message;
                try
                {
                    task.Semaphore.Release();
                }
                catch
                {
                    _logger.LogError("Error liberando semaforo en tarea: " + task.Id);
                }
            }
        }

        //public Task<TaskModel> SendTask(string queue, string json, int timeout)
        //{
        //    TaskModel task = new TaskModel { Id = ++_Id };
        //    task.Semaphore = new SemaphoreSlim(0);

        //    _tasks.Add(task.Id, task);

        //    //enviamos la consulta a la cola adecuada

        //    var tsk = Task.Run<TaskModel>(async () =>
        //    {
        //        try
        //        {
        //            await task.Semaphore.WaitAsync(timeout);
        //        }
        //        finally
        //        {
        //            task.Semaphore.Release();
        //        }

        //        return task;
        //    });

        //    return tsk;
        //}

        public int ReserveTaskId()
        {
            return ++_Id;
        }

        public Task<TaskModel> SendTask(string exchange, string queue, object model, int timeout)
        {
            return SendTask(exchange, queue, model, timeout, 0);
        }
        public async Task<TaskModel> SendTask(string exchange, string queue, object model, int timeout,int reservedTaskId = 0)
        {
            TaskModel task = new TaskModel { Id = reservedTaskId>0? reservedTaskId : ++_Id };
            task.Semaphore = new SemaphoreSlim(0);

            _tasks.Add(task.Id, task);
            //Establecemos el identificador de este mensaje
            (model as ITaskMsgModel).Id = task.Id;

            try
            {
                //Enviamos la peticion rabbit
                using (IModel sendChannel = _rconnection.CreateModel())
                {
                    string json = JsonConvert.SerializeObject(model, _jsonSerializer);

                    sendChannel.BasicPublish(
                        exchange: exchange,
                        routingKey: queue,
                        basicProperties: null,
                        body: Encoding.UTF8.GetBytes(json)
                    );
                }

                //Esperamos a la respuesta
                task.TimedOut = !(await task.Semaphore.WaitAsync(timeout));
            } catch (Exception ex)
            {
                _logger.LogError(ex, "Error en SendTask: " + task.Id);
            }
            finally
            {
                task.Semaphore.Release();
            }
            
            return task;
        }

        public void ResponseTask(int id, string json)
        {
            TaskModel task = null;
            _tasks.TryGetValue(id, out task);

            if (task != null)
            {
                task.Json = json;
                try
                {
                    task.Semaphore.Release();
                }
                catch
                {
                    _logger.LogError("Error liberando semaforo en tarea: " + task.Id);
                }
            }
        }

        public void ResponseTask2(int id, object obj)
        {
            string json = JsonConvert.SerializeObject(obj, _jsonSerializer);

            ResponseTask(id, json);
        }



        public void ReleaseTask(int id)
        {
            _tasks.Remove(id);
        }
    }
}
