﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bananaforscale.Models.Tasks
{
    public class GenerateTextureRequestModel : ITaskMsgModel
    {
        /// <summary>
        /// Id no se establece
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TaskType { get; set; }



        public string Cloth { get; set; }

        public IList<string> Files { get; set; }

        public GenerateTextureRequestModel()
        {
            TaskType = "imagine";
        }
    }

    //public class GenerateTextureResponseModel : ITaskMsgModel
    //{
    //    public int Id { get; set; }
        
    //}
}
