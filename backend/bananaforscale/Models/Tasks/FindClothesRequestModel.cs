﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bananaforscale.Models.Tasks
{
    public class FindClothesRequestModel : ITaskMsgModel
    {
        public int Id { get; set; }
        public string TaskType { get; set; }

        public IList<string> Files { get; set; }

        public FindClothesRequestModel()
        {
            TaskType = "find";
        }
    }
}
