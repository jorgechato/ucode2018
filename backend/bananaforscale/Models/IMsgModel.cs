﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bananaforscale.Models
{
    interface ITaskMsgModel
    {
        int Id { get; set; }

        string TaskType { get; set; }
    }
}
