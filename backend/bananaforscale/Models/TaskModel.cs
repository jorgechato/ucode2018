﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace bananaforscale.Models
{
    public class TaskModel
    {
        public int Id { get; set; }

        //JSON que se devuelve a la peticion
        public string Json { get; set; }

        //marcamos si ha habido un timeout
        public bool TimedOut { get; set; }

        public SemaphoreSlim Semaphore { get; set; } 
    }
}
