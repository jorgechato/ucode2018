#!/usr/bin/env python
import pika
import time
import json
import requests


def main():
    """ Main program """
    credentials = pika.PlainCredentials('alberto', 'woodenCNC')
    parameters = pika.ConnectionParameters('aitgelionr.ddns.net',
                                           5600,
                                           '/woodenCNC',
                                           credentials)
    connection = pika.BlockingConnection(parameters)
    result = connection.channel()
    result.queue_declare(queue='result', auto_delete=True, exclusive=False)

    channel = connection.channel()
    # channel.queue_delete(queue='dimitri')
    # channel.queue_delete(queue='result')
    channel.queue_declare(queue='find', auto_delete=True, exclusive=False)

    imagine = connection.channel()
    # channel.queue_delete(queue='dimitri')
    # channel.queue_delete(queue='result')
    imagine.queue_declare(queue='imagine', auto_delete=True, exclusive=False)
    # find
    print(' [*] Waiting for messages. To exit press CTRL+C')


    def imagineCallback(ch, method, properties, body):
        print(" [x] Received %r" % body)
        # pero hacerla al final!!
        ch.basic_ack(delivery_tag=method.delivery_tag)
        data = json.loads(body)
        print 'Tarea TEXTURE: ' + str(data['id'])
        print data['files']
        url = 'http://aitgelionr.ddns.net/api/dimitri/textures/' + str(data['id']) + '/' + str(data['cloth'])
        print url
        # with open('textura.jpg', 'rb') as f: r = requests.post(url, files={'texture.jpg': f})
        requests.post(url, files=[('files', open('textura.jpg', 'rb')), ('files', open('origen.jpg', 'rb'))])

    def findCallback(ch, method, properties, body):
        print(" [x] Received %r" % body)
        # pero hacerla al final!!
        ch.basic_ack(delivery_tag=method.delivery_tag)
        data = json.loads(body)
        response = {}
        response['id'] = data['id']
        response['taskType'] = data['taskType']

        if data['taskType'] == 'find':
            print 'Tarea FIND: ' + str(response['id'])
            response['files'] = [];
            file1 = {}
            file1['file'] = '45_0.png';
            file1['rois'] = [];
            rois = {}
            rois['cloth'] = "shoe";
            rois['x'] = 0;
            rois['y'] = 0;
            rois['w'] = 100;
            rois['h'] = 100;
            file1['rois'].append(rois);
            rois = {}
            rois['cloth'] = "shirt";
            rois['x'] = 20;
            rois['y'] = 30;
            rois['w'] = 50;
            rois['h'] = 50;
            file1['rois'].append(rois);
            rois = {}
            rois['cloth'] = "shoe";
            rois['x'] = 50;
            rois['y'] = 30;
            rois['w'] = 30;
            rois['h'] = 20;
            file1['rois'].append(rois);
            response['files'].append(file1)
            result.basic_publish(
                exchange='',
                routing_key='result',
                # body='{"code": 200,"message": "Very success","id":'+str(data['id'])+'}'
                body=json.dumps(response)
            )
        elif data['taskType'] == 'imagine':
            print 'Tarea TEXTURE: ' + str(response['id'])
            print data['files']
            url = 'http://aitgelionr.ddns.net/api/dimitri/textures/' + str(data['id']) + '/' + str(data['cloth'])
            print url
            # with open('textura.jpg', 'rb') as f: r = requests.post(url, files={'texture.jpg': f})
            requests.post(url, files=[('files', open('textura.jpg', 'rb')), ('files', open('origen.jpg', 'rb'))])

        print(" [x] Done")
        print(" [x] Sent result")

    channel.basic_consume(findCallback,
                          queue='find')
    imagine.basic_consume(imagineCallback,
                          queue='imagine')

    channel.start_consuming()

    return 0


if __name__ == "__main__":
    main()