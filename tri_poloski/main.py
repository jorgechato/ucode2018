#!/usr/bin/env python
import pika
import time
import json


def main():
    """ Main program """
    credentials = pika.PlainCredentials('alberto', 'woodenCNC')
    parameters = pika.ConnectionParameters('aitgelionr.ddns.net',
                                           5600,
                                           '/woodenCNC',
                                           credentials)
    connection = pika.BlockingConnection(parameters)
    result = connection.channel()
    result.queue_declare(queue='result',auto_delete=True, exclusive=False)
    channel = connection.channel()
    channel.queue_delete(queue='dimitri')
    channel.queue_delete(queue='result')
    channel.queue_declare(queue='dimitri')
    print(' [*] Waiting for messages. To exit press CTRL+C')

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)
        data = json.loads(body)
        print(" [x] Done")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        result.basic_publish(exchange='',
                              routing_key='result',
                              body='{"code": 200,"message": "Very success","id":'+str(data['id'])+'}'
                              )
        print(" [x] Sent result")


    channel.basic_consume(callback,
                          queue='dimitri')

    channel.start_consuming()

    return 0

if __name__ == "__main__":
    main()