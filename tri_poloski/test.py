import pika
import sys

message = ' '.join(sys.argv[1:]) or '{"code": 200,"message": "Very success", "id": 1}'

credentials = pika.PlainCredentials('alberto', 'woodenCNC')
parameters = pika.ConnectionParameters('aitgelionr.ddns.net',
                                   5600,
                                   '/woodenCNC',
                                   credentials)

connection = pika.BlockingConnection(parameters)

channel = connection.channel()

channel.basic_publish(exchange='',
                      routing_key='result',
                      body=message)
print(" [x] Sent %r" % message)
connection.close()