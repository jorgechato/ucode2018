import xml.etree.ElementTree as ET
import requests
import os
import re
from urlparse import urlparse
from bs4 import BeautifulSoup
from PIL import Image
from StringIO import StringIO

DATA_DIR = "data"

import sys
print("The Python version is %s.%s.%s" % sys.version_info[:3])

def parseSitemap(sitemap, ns):
    tree = ET.parse(sitemap)
    nsmap = {"ns": ns}
    root = tree.getroot()
    urls = root.findall('ns:url', nsmap)
    return [url.find('ns:loc', nsmap).text for url in urls]

def parseAdidasProduct(url):
    productUrl = urlparse(url)
    #https://stackoverflow.com/questions/37190978/regex-get-text-before-and-after-a-hyphen
    m = re.search('^[^-]*[^-]', productUrl.path)
    productDirectory = DATA_DIR + '/adidas' + m.group(0) + '/'

    if not os.path.exists(productDirectory):
        os.makedirs(productDirectory)
    r = requests.get(url)
    soup = BeautifulSoup(r.content, "html.parser")
    images = []
    for img in soup.select("div[class^=thumbnail___] img"):
        imgUrl = urlparse(img.get('src'))
        if "detail" in os.path.basename(imgUrl.path):
            continue
        if "video" in os.path.basename(imgUrl.path):
            continue

        imgFileName = productDirectory + os.path.basename(imgUrl.path)

        if os.path.exists(imgFileName):
            continue
        print imgFileName
        image_request_result = requests.get('http://' + imgUrl.scheme+imgUrl.netloc+imgUrl.path)
        if image_request_result.status_code is not 200:
            continue

        print imgFileName
        imageData = StringIO(image_request_result.content)
        image = Image.open(imageData)
        width, height = image.size
        max_size = [512, 512]
        if width > 512 or height > 512:
            image.thumbnail(max_size)

        image.save(imgFileName, format='JPEG')

    with open('adidas_visited.urls', 'a') as file:
        file.write(url+"\n")

def main():
    """ Main program """
    if not os.path.exists(DATA_DIR):
        os.mkdir(DATA_DIR)

    if not os.path.exists(DATA_DIR + '/adidas'):
        os.mkdir(DATA_DIR + '/adidas')

    urls = parseSitemap('adidas-ES-es-es-product.xml', "http://www.sitemaps.org/schemas/sitemap/0.9")
    with open('adidas_visited.urls') as f:
        visited_urls = f.readlines()
    print visited_urls
    for url in urls:
        if url+"\n" in visited_urls:
            print 'Skipped ' + url
            continue
        parseAdidasProduct(url)
    return 0

if __name__ == "__main__":
    main()