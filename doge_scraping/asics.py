import xml.etree.ElementTree as ET
import requests
import os
import re
from urlparse import urlparse
from bs4 import BeautifulSoup
from PIL import Image
from StringIO import StringIO

DATA_DIR = "data"

import sys
print("The Python version is %s.%s.%s" % sys.version_info[:3])

def parseSitemap(sitemap, ns):
    tree = ET.parse(sitemap)
    nsmap = {"ns": ns}
    root = tree.getroot()
    urls = root.findall('ns:url', nsmap)
    return [url.find('ns:loc', nsmap).text for url in urls]

def parseAsicsProduct(url):
    productUrl = urlparse(url)
    #https://stackoverflow.com/questions/37190978/regex-get-text-before-and-after-a-hyphen
    #m = re.search('^[^-]*[^-]', productUrl.path)
    productDirectory = DATA_DIR + '/asics' #+ m.group(0) + '/'

    if not os.path.exists(productDirectory):
        os.makedirs(productDirectory)
    r = requests.get(url)
    soup = BeautifulSoup(r.content, "html.parser")
    images = []
    print url

    for img in soup.select("img[class=product-img]"):
        imgUrl = urlparse(img.get('data-big'))

        imgFileName = productDirectory + '/' + os.path.basename(imgUrl.path)

        if os.path.exists(imgFileName):
            continue
        image_request_result = requests.get('http://' +imgUrl.netloc+imgUrl.path)
        if image_request_result.status_code is not 200:
            continue

        print imgFileName
        imageData = StringIO(image_request_result.content)
        image = Image.open(imageData)
        width, height = image.size
        max_size = [512, 512]
        if width > 512 or height > 512:
            image.thumbnail(max_size)

        image.save(imgFileName, format='JPEG')
    with open('asics_visited.urls', 'a') as file:
        file.write(url+"\n")

def main():
    """ Main program """
    if not os.path.exists(DATA_DIR):
        os.mkdir(DATA_DIR)

    if not os.path.exists(DATA_DIR + '/asics'):
        os.mkdir(DATA_DIR + '/asics')

    urls = parseSitemap('asics-PRODUCT-en-USD.xml', "http://www.sitemaps.org/schemas/sitemap/0.9")
    if not os.path.exists('asics_visited.urls'):
        open('asics_visited.urls', 'a').close()
    with open('asics_visited.urls') as f:
        visited_urls = f.readlines()
    print visited_urls
    for url in urls:
        if url+"\n" in visited_urls:
            print 'Skipped ' + url
            continue
        parseAsicsProduct(url)
    return 0

if __name__ == "__main__":
    main()