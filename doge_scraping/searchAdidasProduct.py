import xml.etree.ElementTree as ET
import requests
import os
import re
from urlparse import urlparse
from bs4 import BeautifulSoup
from PIL import Image
from StringIO import StringIO

DATA_DIR = "data"

import sys
print("The Python version is %s.%s.%s" % sys.version_info[:3])

def parseSitemap(sitemap, ns):
    tree = ET.parse(sitemap)
    nsmap = {"ns": ns}
    root = tree.getroot()
    urls = root.findall('ns:url', nsmap)
    return [url.find('ns:loc', nsmap).text for url in urls]

def searchAdidasProduct(url):
    productUrl = urlparse(url)
    print url
    #https://stackoverflow.com/questions/37190978/regex-get-text-before-and-after-a-hyphen
    if "camiseta" in url:
        with open('camisetas_adidas.urls', 'a') as file:
            file.write(os.path.basename(productUrl.path)[:-5]+"\n")

def main():
    """ Main program """
    if not os.path.exists(DATA_DIR):
        os.mkdir(DATA_DIR)

    if not os.path.exists(DATA_DIR + '/adidas'):
        os.mkdir(DATA_DIR + '/adidas')

    urls = parseSitemap('adidas-ES-es-es-product.xml', "http://www.sitemaps.org/schemas/sitemap/0.9")

    for url in urls:
        searchAdidasProduct(url)
    return 0

if __name__ == "__main__":
    main()