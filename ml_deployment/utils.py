from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import shutil
from super_resolve import super_sample
from gan_gen import generate
import utils
import torch


def mkdir(paths):
    if not isinstance(paths, (list, tuple)):
        paths = [paths]
    for path in paths:
        if not os.path.isdir(path):
            os.makedirs(path)


def cuda_devices(gpu_ids):
    gpu_ids = [str(i) for i in gpu_ids]
    os.environ['CUDA_VISIBLE_DEVICES'] = ','.join(gpu_ids)


def cuda(xs):
    if torch.cuda.is_available():
        if not isinstance(xs, (list, tuple)):
            return xs.cuda()
        else:
            a = [x.cuda() for x in xs]
            print(a)
            return a
    else:
        return xs


def save_checkpoint(state, save_path, is_best=False, max_keep=None):
    # save checkpoint
    torch.save(state, save_path)

    # deal with max_keep
    save_dir = os.path.dirname(save_path)
    list_path = os.path.join(save_dir, 'latest_checkpoint')

    save_path = os.path.basename(save_path)
    if os.path.exists(list_path):
        with open(list_path) as f:
            ckpt_list = f.readlines()
            ckpt_list = [save_path + '\n'] + ckpt_list
    else:
        ckpt_list = [save_path + '\n']

    if max_keep is not None:
        for ckpt in ckpt_list[max_keep:]:
            ckpt = os.path.join(save_dir, ckpt[:-1])
            if os.path.exists(ckpt):
                os.remove(ckpt)
        ckpt_list[max_keep:] = []

    with open(list_path, 'w') as f:
        f.writelines(ckpt_list)

    # copy best
    if is_best:
        shutil.copyfile(save_path, os.path.join(save_dir, 'best_model.ckpt'))


def load_checkpoint(ckpt_dir_or_file, map_location=None, load_best=False):
    if os.path.isdir(ckpt_dir_or_file):
        if load_best:
            ckpt_path = os.path.join(ckpt_dir_or_file, 'best_model.ckpt')
        else:
            with open(os.path.join(ckpt_dir_or_file, 'latest_checkpoint')) as f:
                ckpt_path = os.path.join(ckpt_dir_or_file, f.readline()[:-1])
    else:
        ckpt_path = ckpt_dir_or_file
    use_cuda = torch.cuda.is_available()
    if use_cuda:
        ckpt = torch.load(ckpt_path, map_location=map_location)
    else:
        ckpt = torch.load(ckpt_path, map_location=lambda storage, loc: storage)
    print(' [*] Loading checkpoint from %s succeed!' % ckpt_path)
    return ckpt

def new_shoes(Nshoes, z):
    Model_shoes = './model_saves/shoes_Epoch_78.ckpt'
    Model_super = './model_saves/shoes512_epoch_30.pth'

    lowsample_dir = './out/shoe/lowsample_imgs'
    supersample_dir = './out/shoe/supersample_imgs'

    utils.mkdir(lowsample_dir)
    utils.mkdir(supersample_dir)

    generate(Model_shoes, Nshoes, lowsample_dir, z)

    for i in range(Nshoes):
        input_name = lowsample_dir + '/%d.jpg' % i
        output_name = supersample_dir + '/%d.png' % i
        super_sample(Model_super, input_name, output_name)

def new_shirts(Nshoes, z):
    Model_shoes = './model_saves/clothes_Epoch_61.ckpt'
    Model_super = './model_saves/shirts512_epoch_30.pth'

    lowsample_dir = './out/shirt/lowsample_imgs'
    supersample_dir = './out/shirt/supersample_imgs'

    utils.mkdir(lowsample_dir)
    utils.mkdir(supersample_dir)

    generate(Model_shoes, Nshoes, lowsample_dir, z)

    for i in range(Nshoes):
        input_name = lowsample_dir + '/%d.jpg' % i
        output_name = supersample_dir + '/%d.png' % i
        super_sample(Model_super, input_name, output_name)

