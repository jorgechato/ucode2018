from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import models_64x64
import PIL.Image as Image
import tensorboardX
import torch
from torch.autograd import grad
from torch.autograd import Variable
import torch.nn as nn
import torchvision
import torchvision.datasets as dsets
import torchvision.transforms as transforms
import utils
import sys
import numpy as np


def generate(model_file, batch_size, save_dir, z=None):

    if z is None:
        z = np.random.randn((batch_size, 100))

    z_dim = z.shape[0]

    """ model """
    D = models_64x64.DiscriminatorWGANGP(3)
    G = models_64x64.Generator(z_dim)
    bce = nn.BCEWithLogitsLoss()
    utils.cuda([D, G, bce])

    lr = 0.001
    d_optimizer = torch.optim.Adam(D.parameters(), lr=lr, betas=(0.5, 0.999))
    g_optimizer = torch.optim.Adam(G.parameters(), lr=lr, betas=(0.5, 0.999))


    """ load checkpoint """
    ckpt_file = model_file

    ckpt = utils.load_checkpoint(ckpt_file)
    start_epoch = ckpt['epoch']
    D.load_state_dict(ckpt['D'])
    G.load_state_dict(ckpt['G'])
    d_optimizer.load_state_dict(ckpt['d_optimizer'])
    g_optimizer.load_state_dict(ckpt['g_optimizer'])

    # set train
    G.eval()
    bs = batch_size
    z = Variable(torch.from_numpy(z))
    r_lbl = Variable(torch.ones(bs))
    f_lbl = Variable(torch.zeros(bs))
    z = utils.cuda(z)

    f_imgs_sample = (G(z).data + 1) / 2.0

    save_dir = './save_imgs'
    utils.mkdir(save_dir)
    for i in range(f_imgs_sample.shape[0]):
        img = f_imgs_sample[i]
        torchvision.utils.save_image(img, '%s/%d.jpg' % (save_dir, i), nrow=8)


#generate('../model_saves/shoes_Epoch_48.ckpt', 10)