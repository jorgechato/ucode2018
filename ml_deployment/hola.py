#!/usr/bin/env python
import pika
import time
import json
import requests
import random
from os import listdir
from os.path import isfile, join
from utils import new_shirts, new_shoes

shoepath = './out/present_shoes'
shirtpath = './out/good_shirts_gan'

def main():
    """ Main program """
    credentials = pika.PlainCredentials('alberto', 'woodenCNC')
    parameters = pika.ConnectionParameters('aitgelionr.ddns.net',
                                           5600,
                                           '/woodenCNC',
                                           credentials)
    connection = pika.BlockingConnection(parameters)
    result = connection.channel()
    result.queue_declare(queue='result', auto_delete=True, exclusive=False)

    imagine = connection.channel()
    imagine.queue_declare(queue='imagine', auto_delete=True, exclusive=False)
    # find
    print(' [*] Waiting for messages. To exit press CTRL+C')

    def imagineCallback(ch, method, properties, body):
        print(" [x] Received %r" % body)
        # pero hacerla al final!!
        ch.basic_ack(delivery_tag=method.delivery_tag)
        data = json.loads(body)
        print 'Tarea TEXTURE: ' + str(data['id'])
        print data['files']
        url = 'http://aitgelionr.ddns.net/api/dimitri/textures/' + str(data['id']) + '/' + str(data['cloth'])
        print url
        # with open('textura.jpg', 'rb') as f: r = requests.post(url, files={'texture.jpg': f})
        # ('files', open('textura.jpg', 'rb')), ('files', open('origen.jpg', 'rb'))

        z = data['z']

        Nims = 4
        response_files = []

        #print(data['cloth'] == 'shoe')

        if data['cloth'] == 'shoe':
            new_shoes(Nims, z)
            onlyfiles = [f for f in listdir(shoepath) if isfile(join(shoepath, f))]
            o_path = shoepath

        elif data['cloth'] == 'shirt':
            new_shirts(Nims, z)
            onlyfiles = [f for f in listdir(shirtpath) if isfile(join(shirtpath, f))]
            o_path = shirtpath

        for i in range(Nims):
            random.shuffle(onlyfiles)
            filename = onlyfiles[i]
            response_files.append(('files', open(o_path + '/' + filename, 'rb')))

        requests.post(url, files=response_files)

    imagine.basic_consume(imagineCallback,
                          queue='imagine')

    imagine.start_consuming()

    return 0


if __name__ == "__main__":
    main()