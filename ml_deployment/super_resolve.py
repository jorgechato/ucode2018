from __future__ import print_function
import argparse
import torch
from torch.autograd import Variable
from PIL import Image
from torchvision.transforms import ToTensor

import numpy as np


def super_sample(model, input_image, output_filename):

    img = Image.open(input_image).convert('YCbCr')
    y, cb, cr = img.split()
    use_cuda = torch.cuda.is_available()

    print(model)

    if use_cuda:
        model = torch.load(model)
    else:
        model = torch.load(model, map_location=lambda storage, loc: storage)


    input = Variable(ToTensor()(y)).view(1, -1, y.size[1], y.size[0])

    if use_cuda:
        model = model.cuda()
        input = input.cuda()

    out = model(input)
    out = out.cpu()
    out_img_y = out.data[0].numpy()
    out_img_y *= 255.0
    out_img_y = out_img_y.clip(0, 255)
    out_img_y = Image.fromarray(np.uint8(out_img_y[0]), mode='L')

    out_img_cb = cb.resize(out_img_y.size, Image.BICUBIC)
    out_img_cr = cr.resize(out_img_y.size, Image.BICUBIC)
    out_img = Image.merge('YCbCr', [out_img_y, out_img_cb, out_img_cr]).convert('RGB')
    out_img.save(output_filename)
