from super_resolve import super_sample
from gan_gen import generate
import utils

Nshoes = 1

Model_shoes = './model_saves/shoes_Epoch_78.ckpt'
Model_super = './model_saves/shoes512_epoch_30.pth'

lowsample_dir = './out/shoe/lowsample_imgs'
supersample_dir = './out/shoe/supersample_imgs'

utils.mkdir(lowsample_dir)
utils.mkdir(supersample_dir)

generate(Model_shoes, Nshoes, lowsample_dir)

for i in range(Nshoes):
    input_name = lowsample_dir + '/%d.jpg' % i
    output_name = supersample_dir + '/%d.png' % i
    super_sample(Model_super, input_name, output_name)

