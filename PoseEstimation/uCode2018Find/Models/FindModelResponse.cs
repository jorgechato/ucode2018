﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uCode2018Find.Models
{
    public class FindModelResponse : ITaskMsgModel
    {
        public int Id { get; set; }
        public string TaskType { get; set; }

        public List<ImageModel> Files { get; set; }
    }
}
