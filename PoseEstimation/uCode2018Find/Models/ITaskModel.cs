﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uCode2018Find.Models
{
    interface ITaskMsgModel
    {
        int Id { get; set; }

        string TaskType { get; set; }
    }
}
