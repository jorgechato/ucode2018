﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uCode2018Find.Models
{
    public class ImageModel
    {
        public string File { get; set; }
        public List<RoiModel> Rois { get; set; }
    }
}
