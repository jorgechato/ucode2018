﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uCode2018Find.Models
{
    public class RoiModel
    {
        public string Cloth { get; set; }

        public double x { get; set; }
        public double y { get; set; }

        public double w { get; set; }
        public double h { get; set; }
    }
}
