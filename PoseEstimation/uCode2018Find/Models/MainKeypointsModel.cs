﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uCode2018Find.Models
{
    public class MainKeypointsModel
    {
        public double version { get; set; }
        public List<PersonModel> people { get; set; }
    }

    public class PersonModel
    {
        public List<double> pose_keypoints { get; set; }
        public List<double> face_keypoints { get; set; }
        public List<double> hand_left_keypoints { get; set; }
        public List<double> hand_right_keypoints { get; set; }
    }
    
}
