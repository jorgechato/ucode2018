﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using uCode2018Find.Models;

namespace uCode2018Find
{
    class FindWorker
    {

        ConnectionFactory _rfactory;
        IConnection _rconnection;

        //Respuestas
        IModel _responseChannel;

        EventingBasicConsumer _rconsumer;

        JsonSerializerSettings _jsonSerializer;

        Regex _idRegex;

        //Cola
        ConcurrentQueue<FindClothesRequestModel> _queue = new ConcurrentQueue<FindClothesRequestModel>();

        public Task Hilo { get; set; }

        public string Hostname { get; set; }

        public bool Exit {get;set;}
        
        public FindWorker(string hostname)
        {
            Hostname = hostname;

            _rfactory = new ConnectionFactory
            {
                //Servidor
                //HostName = "localhost",
                HostName = Hostname,
                Port = 5600,

                VirtualHost = "/woodenCNC",

                //Credenciales
                UserName = "pi",
                Password = "woodenCNC"
            };

            _idRegex = new Regex("\"id\" *: *(\\d+)", RegexOptions.Compiled);

            _jsonSerializer = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            InitRabbit();
        }

        public void Run()
        {
            //Preparar las colas

            Hilo = Task.Run(() => {
                while (!Exit)
                {
                    Console.Write(".");
                    Thread.Sleep(1000);

                    ProcessTask();
                }
                Console.WriteLine("Clossing connection...");
                _responseChannel.Close();
                Console.WriteLine("Closed.");
            });


        }

        private void InitRabbit()
        {
            //Creamos los channels, etc
            _rconnection = _rfactory.CreateConnection();
            _responseChannel = _rconnection.CreateModel();

            //Declaramos el exchange
            //_rchannel.ExchangeDeclare("response", ExchangeType.Fanout, false);

            //Declaramos la cola de respuestas
            _responseChannel.QueueDeclare(
                queue: "find",
                //durable: false,                    
                exclusive: false,
                autoDelete: true
            //arguments: null
            );

            //Preparamos el consumer
            _rconsumer = new EventingBasicConsumer(_responseChannel);
            _responseChannel.BasicConsume("find", true, _rconsumer);

            _rconsumer.Received += ProcessRabbitMsg;
        }

        private void cleanDir(string dirPath)
        {
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
            var files = Directory.GetFiles(dirPath);
            foreach(var file in files)
            {
                File.Delete(file);
            }
        }

        private void ProcessRabbitMsg(object model, BasicDeliverEventArgs ea)
        {
            //Obtenemos el JSON devuelto
            var message = Encoding.UTF8.GetString(ea.Body);

            FindClothesRequestModel task = JsonConvert.DeserializeObject<FindClothesRequestModel>(message);

            _queue.Enqueue(task);
        }

        private async Task ProcessTask()
        {
            FindClothesRequestModel task = null;
            _queue.TryDequeue(out task);
            if (task == null) return;


            Console.WriteLine(Environment.NewLine + "Processing task: " + task.Id);

            //Obtenemos los directorios
            string inFolder = Path.Combine("toproc", task.Id.ToString());
            string toFolder = Path.Combine("outdata", task.Id.ToString());

            cleanDir(inFolder);
            cleanDir(toFolder);

            //Obtenemos el identificador de la tarea
            HttpClient client = new HttpClient();

            //descargamos las imagenes
               
            foreach (var file in task.Files)
            {
                string filePath = Path.Combine(inFolder, file);
                try
                {
                    using (var stream = await client.GetStreamAsync($"http://{Hostname}/shared/{file}"))
                    using(var fo = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        await stream.CopyToAsync(fo);
                    }
                }
                catch
                {

                }
            }

            //Lanzamos el procesado
            try
            {
                string openpose = "OpenPoseDemo.exe";
                //var process = Process.Start(openpose, $"--image_dir {inFolder} --write_json {toFolder} --write_images output_img");
                var process = Process.Start(openpose, $"--image_dir {inFolder} --write_json {toFolder}");
                //var process = Process.Start(openpose, $"--image_dir {inFolder} --write_keypoint_json {toFolder} --write_images output_img");
                if (!process.WaitForExit(10000)) return;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al lanzar openpose", ex.Message);
            }
            //parseamos los json de salida
            var outjsons = Directory.GetFiles(toFolder);
            var response = new List<ImageModel>();

            foreach(var file in outjsons)
            {
                //Obtenemos el nombre original
                string ofname = Path.GetFileName(file);
                int pos = ofname.IndexOf("_keypoints");
                ofname = ofname.Substring(0, pos);

                ofname = task.Files.FirstOrDefault(x => x.Contains(ofname));
                if(ofname == null)
                {
                    Console.WriteLine("No se encontro: " + file);
                    continue;
                }

                //Obtenemos los esqueletos
                var rfile = new ImageModel { File =  ofname, Rois = new List<RoiModel>() };

                try
                {
                    var main = LoadJson(File.ReadAllText(file));

                    foreach (var person in main.people)
                    {
                        rfile.Rois.Add(GetShirt(person));
                        rfile.Rois.Add(GetTrouser(person));
                        rfile.Rois.Add(GetShoes(person, false));
                        rfile.Rois.Add(GetShoes(person, true));
                    }
                    response.Add(rfile);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error cargado JSON: " , ex.Message);
                }
            }

            //TODO: enviamos la respuesta a la cola de respuesta
            using (IModel sendChannel = _rconnection.CreateModel())
            {
                FindModelResponse obj = new FindModelResponse
                {
                     Id = task.Id,
                     TaskType = task.TaskType,
                     Files = response
                };
                string json = JsonConvert.SerializeObject(obj, _jsonSerializer);

                sendChannel.BasicPublish(
                    exchange: "",
                    routingKey: "result",
                    basicProperties: null,
                    body: Encoding.UTF8.GetBytes(json)
                );
            }
        }

        //parse keypoints
        MainKeypointsModel LoadJson(string json)
        {
            var model = JsonConvert.DeserializeObject<MainKeypointsModel>(json);
            return model;
        }

        RoiModel GetShirt(PersonModel person)
        {
            RoiModel roi = new RoiModel { Cloth = "shirt" };

            //Hombros
            var hi = GetCoords(person, 2);
            var hd = GetCoords(person, 5);

            //caderas
            var ci = GetCoords(person, 8);
            var cd = GetCoords(person, 11);

            //pies
            //var pi = GetCoords(person, 10);
            //var pd = GetCoords(person, 13);

            //X
            roi.x = Math.Min(hi.x, hd.x);
            roi.x = Math.Min(roi.x, ci.x);
            roi.x = Math.Min(roi.x, cd.x);

            double x2 = Math.Max(hi.x, hd.x);
            x2 = Math.Max(roi.x, ci.x);
            x2 = Math.Max(roi.x, cd.x);

            roi.w = Math.Abs(roi.x - x2);

            //Y
            roi.y = Math.Min(hi.y, hd.y);
            roi.y = Math.Min(roi.y, ci.y);
            roi.y = Math.Min(roi.y, cd.y);

            double y2 = Math.Max(hi.y, hd.y);
            y2 = Math.Max(y2, ci.y);
            y2 = Math.Max(y2, cd.y);

            roi.h = Math.Abs(roi.y - y2);

            ScaleRoi(roi, 0.10);

            return roi;
        }

        RoiModel GetTrouser(PersonModel person)
        {
            RoiModel roi = new RoiModel { Cloth = "trouser" };

            //Hombros
            //var hi = GetCoords(person, 2);
            //var hd = GetCoords(person, 5);

            //caderas
            var ci = GetCoords(person, 8);
            var cd = GetCoords(person, 11);

            //rodillas
            var ri = GetCoords(person, 9);
            var rd = GetCoords(person, 12);

            //pies
            var pi = GetCoords(person, 10);
            var pd = GetCoords(person, 13);

            //X
            roi.x = Math.Min(pi.x, pd.x);
            roi.x = Math.Min(roi.x, ci.x);
            roi.x = Math.Min(roi.x, cd.x);
            roi.x = Math.Min(roi.x, rd.x);
            roi.x = Math.Min(roi.x, ri.x);

            double x2 = Math.Max(pi.x, pd.x);
            x2 = Math.Max(x2, ci.x);
            x2 = Math.Max(x2, cd.x);
            x2 = Math.Max(x2, pd.x);
            x2 = Math.Max(x2, pi.x);
            x2 = Math.Max(x2, rd.x);
            x2 = Math.Max(x2, ri.x);

            roi.w = Math.Abs(roi.x - x2);

            //Y
            roi.y = Math.Min(pi.y, pd.y);
            roi.y = Math.Min(roi.y, ci.y);
            roi.y = Math.Min(roi.y, cd.y);

            double y2 = Math.Max(pi.y, pd.y);
            y2 = Math.Max(y2, ci.y);
            y2 = Math.Max(y2, cd.y);

            roi.h = Math.Abs(roi.y - y2);

            ScaleRoi(roi, 0.10);

            return roi;
        }



        RoiModel GetShoes(PersonModel person, bool esIzquierdo)
        {
            RoiModel roi = new RoiModel { Cloth = "shoe" };
            
            //rodillas
            var ri = GetCoords(person, 9);
            var rd = GetCoords(person, 12);

            //pies
            var pi = GetCoords(person, 10);
            var pd = GetCoords(person, 13);

            if (esIzquierdo)
            {
                roi.x = pi.x;
                roi.y = pi.y;
            }
            else
            {
                roi.x = pd.x;
                roi.y = pd.y;
            }

            double d = dist(pi.x, pi.y, ri.x, ri.y);
            double d2 = dist(pd.x, pd.y, rd.x, rd.y);

            d = Math.Max(d, d2);

            roi.x -= d * 0.10;
            //roi.y -= d * 0.05;

            roi.w = d * 0.30;
            roi.h = d * 0.35;

            ScaleRoi(roi, 0.15);

            return roi;
        }

        (double x, double y) GetCoords(PersonModel person, int id)
        {

            double x = person.pose_keypoints[id * 3];
            double y = person.pose_keypoints[id * 3  + 1];

            return (x, y);
        }

        double dist(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
        }

        void ScaleRoi(RoiModel roi, double scale)
        {
            double d = roi.w * scale;

            roi.x -= d;
            roi.y -= d;
            roi.w += d * 2;
            roi.h += d * 2;
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            var worker = new FindWorker("192.168.1.202");

            Directory.SetCurrentDirectory(@"D:\Usuarios\Aitor\Descargas\openpose-1.2.1-win64-binaries\openpose-1.2.1-win64-binaries");

            Console.WriteLine("Running on: " + Environment.CurrentDirectory);

            worker.Run();

            Console.WriteLine("Ejecutando...");
            Console.WriteLine("- Pulsa una tecla para salir -");
            Console.ReadKey();
            worker.Exit = true;
            Console.Write("Espere a cerrar la conexion");
            Console.ReadKey();
        }
    }
}
